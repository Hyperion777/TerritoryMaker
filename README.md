# Territory Maker
Questo progetto serve per creare i territori di una congregazione.
La creazione si svolge in due fasi:
1. Delineamento dei singoli territori sulla base del numero di famiglie (conteggio.py)
2. Stampa dei territori (rendering.py)

## Prerequisiti
- Scarica questo pacchetto ([link](https://gitlab.com/Hyperion777/TerritoryMaker/repository/archive.zip?ref=master))
- Python (scaricabile da qui: [32 bit](https://www.python.org/ftp/python/3.6.0/python-3.6.0.exe) / [64 bit](https://www.python.org/ftp/python/3.6.0/python-3.6.0-amd64.exe))
- Google Earth Pro ([scarica](https://www.google.it/earth/download/gep/agree.html))

## Download area di OpenStreetMap
Andare in [OpenStreetMap](https://www.openstreetmap.org) e premere il pulsante in alto "Scarica".
Probabilmente la mappa sarà da scaricare da un mirror di OpenStreetMap (come Overpass API).
Il file scaricato andrà messo nella cartella "Sources" e dovrà essere rinominato con l'estensione .osm

## Modifiche alle mappe di OpenStreetMap
Potete modificare le mappe andando sul [sito](https://www.openstreetmap.org) e loggandovi.
Quando ci si trova in modifica, ci sono numerosi link a documentazione e wiki, che spiegano le "best practice" sul come farlo.

## Script
All'interno della cartella ci sono due script:

### conteggio.py
Questo script:
1. Apre un file KML
2. Rileva tutti i segnaposto inseriti nel KML

### rendering.py
Questo script:
1. Apre un file KML
2. Renderizza utilizzando Maperitive i poligoni (aree) visibili all'interno del KML