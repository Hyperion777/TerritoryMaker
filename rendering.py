"""
Script per l'estrazione dei poligoni da un file KML e la renderizzazione con Maperitive
Data creazione: 24/12/2015
Data ultima modifica: 07/05/2017
Autore: Alessandro Lucchet
"""
import tempfile
import sys
import os
import glob

# Costanti
SCRIPT_PATH=os.path.dirname(os.path.realpath(__file__))+'\\'
POLYGON_TAG = "Polygon"
COORDS_TAG = "coordinates"
NAME_TAG = "name"
VISIBILITY_TAG = "visibility"
PLACEMARK_TAG = "Placemark"
TEMP_PATH = tempfile.gettempdir()+"\\kml2osm2png\\"
RULES_PATH = SCRIPT_PATH+"Rules\\"
MAP_SOURCE = SCRIPT_PATH+"Sources\\"
EXPORT_OPTIONS = "dpi=300 width=1920 height=1200"
RENDERED_PATH=SCRIPT_PATH+'Rendered\\'
COLOR_PARAM = '\033[92m'
COLOR_END = '\033[0m'

# include
from xml.dom.minidom import parse,Document
from subprocess import call

# se serve, crea la cartella temporanea
if not os.path.exists(TEMP_PATH):
    os.makedirs(TEMP_PATH)

# inizio programma
os.system("cls")
print("Questo script:\n1. Apre un file KML\n2. Renderizza utilizzando Maperitive i poligoni (aree) visibili all'interno del KML\n\nQuesti sono i parametri")
print("Qualità e dimensione dei file esportati: "+COLOR_PARAM+EXPORT_OPTIONS+COLOR_END)
print("Percorso nel quale verranno renderizzati i territori: "+COLOR_PARAM+RENDERED_PATH+COLOR_END)
print("Percorso contenente le regole di rendering: "+COLOR_PARAM+RULES_PATH+COLOR_END)
print("Percorso dei file OSM che verranno caricati come sfondo: "+COLOR_PARAM+MAP_SOURCE+COLOR_END+"\nI file OSM li puoi scaricare da www.openstreetmap.org o overpass-api.de")
print()
if len(sys.argv)>1:
	filekml=sys.argv[1]
	print("Utilizzerò il seguente file KML: "+COLOR_PARAM+filekml+COLOR_END+"\n")
else:
	#print "Inserisci il nome del file da aprire: ",
	#filekml = input()
	filekml = os.getenv('LOCALAPPDATA')+"Low\\Google\\GoogleEarth\\myplaces.kml"
	print("Utilizzerò il file KML di Google Earth \"I miei luogi\".\nQuesto file si trova in: "+COLOR_PARAM+filekml+COLOR_END+"\nIn alternativa puoi trascinare sopra questo script il file KML da renderizzare\n")

# richiedi le regole da utilizzare
print("Seleziona le regole per il rendering che vuoi utilizzare ")
rules = glob.glob(RULES_PATH+"*.mrules")
for index,rule in enumerate(rules):
	print(index,">",os.path.basename(rule))
while True:
	number = input("Inserisci il numero della regola: ")
	if number.isnumeric() and int(number)<len(rules):
		break
rule = rules[int(number)]

# estraggo i poligoni
print("Estrazione dei poligoni... ")
xmldoc = parse(filekml)
list = xmldoc.getElementsByTagName(PLACEMARK_TAG)
count = 0
osmnames = []  #dichiaro la lista
for placemark in list:
	if len(placemark.getElementsByTagName(POLYGON_TAG))>0 and (len(placemark.getElementsByTagName(VISIBILITY_TAG))==0 or placemark.getElementsByTagName(VISIBILITY_TAG)[0].firstChild.nodeValue==1):
		count += 1

		# inizio a creare un file osm con il poligono indicato
		doc = Document()
		root = doc.createElement('osm')
		root.attributes['version']='0.6'
		root.attributes['generator']='HypGenerator'

		# creo la way del file osm
		way = doc.createElement('way')
		way.attributes['uid']=str(-1)
		way.attributes['id']=str(1)

		# aggiungo il tag area
		tag = doc.createElement('tag')
		tag.attributes['k']='area'
		tag.attributes['v']='yes'
		way.appendChild(tag)

		# aggiungo il tag territorio
		tag = doc.createElement('tag')
		tag.attributes['k']='territorio'
		tag.attributes['v']='yes'
		way.appendChild(tag)

		# aggiungo il tag nome (se c'e')
		if len(placemark.getElementsByTagName(NAME_TAG))>0:
			name=placemark.getElementsByTagName(NAME_TAG)[0].firstChild.nodeValue
		else:
			if len(placemark.parentNode.getElementsByTagName(NAME_TAG))>0:
				name=placemark.parentNode.getElementsByTagName(NAME_TAG)[0].firstChild.nodeValue
			else:
				name='poligono_'+str(count)
		tag = doc.createElement('tag')
		tag.attributes['k']='name'
		tag.attributes['v']=name#encode('utf-8')
		way.appendChild(tag)
		print(name)

		# scorro i vertici
		vertici=placemark.getElementsByTagName(COORDS_TAG)[0].firstChild.nodeValue.split()
		vertice_count=0
		for vertice in vertici:
			# aggiungo l'elemento che contiene le coordinate
			vertice_count += 1
			coordinata=vertice.split(',')
			node=doc.createElement('node')
			node.attributes['uid']=str(-1)
			node.attributes['id']=str(vertice_count)
			node.attributes['lat']=str(coordinata[1])
			node.attributes['lon']=str(coordinata[0])
			root.appendChild(node)

			# lo aggiungo al way
			nd=doc.createElement('nd')
			nd.attributes['ref']=str(vertice_count)
			way.appendChild(nd)

		# chiudo l'area ri-aggiungendo il primo vertice
		nd=doc.createElement('nd')
		nd.attributes['ref']=str(1)
		way.appendChild(nd)

		# aggiungo al file osm, il nodo way
		root.appendChild(way)

		# scrivo il file
		nomefile=TEMP_PATH+name+'.osm'
		osmnames.append(name)
		root.writexml(open(nomefile, 'w'),indent="",addindent="  ",newl='\n')

# ora devo creare la script di maperitive per il rendering dei file osm
scriptfilename=TEMP_PATH+'script.mscript'
scriptfile = open(scriptfilename,'w')
scriptfile.write('use-ruleset location='+rule+'\n')
scriptfile.write('apply-ruleset\n')
scriptfile.write('set-setting name=map.decoration.grid value=False\n')

# carica tutti i file osm
for osm in osmnames:
	scriptfile.write('load-source "'+TEMP_PATH+osm+'.osm'+'"\n') #osm.encode('utf-8')
	scriptfile.write('set-source visible=false\n')

# carico la mappa sopra i territorio
for file in glob.glob(MAP_SOURCE+"*.osm"):
	scriptfile.write('load-source '+file+'\n')

# esporta tutti i file osm
for osm in osmnames:
	scriptfile.write('set-source index=1 visible=true\n')
	scriptfile.write('geo-bounds-use-source 1\n')
	scriptfile.write('zoom-bounds\n')
	scriptfile.write('export-bitmap file="'+RENDERED_PATH+osm+'.png" '+EXPORT_OPTIONS+'\n') #osm.encode('utf-8')
	scriptfile.write('remove-source 1\n')

# chiudo il file ed eseguo la script
scriptfile.close()
call(SCRIPT_PATH+"Maperitive.Console.exe "+scriptfilename)

# elimino i file temporanei
for file in glob.glob(RENDERED_PATH+"*.georef"):
    os.remove(file)
for file in glob.glob(TEMP_PATH+"*.osm"):
    os.remove(file)
for file in glob.glob(TEMP_PATH+"*.mscript"):
    os.remove(file)

# avviso che ho finito
print("Territori esportati qui: "+COLOR_PARAM+RENDERED_PATH+COLOR_END)
os.system('pause')
